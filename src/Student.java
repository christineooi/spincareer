public class Student
{
    //fields
    public String firstName;
    public String lastName;
    public String gender;

    //constructor
    public Student(String aFirstName, String aLastName, String aGender)
    {
        firstName = aFirstName;
        lastName = aLastName;
        gender = aGender;
    }
}
